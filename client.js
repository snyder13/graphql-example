'use strict';
const fs = require('fs');
const path = require('path');
const request = require('request-promise-native');

function usage() {
	const me = process.argv.slice(0, 2).map((fn) => path.basename(fn)).join(' ');
	console.log(`Usage:

Query as an argument:
	${me} '{ query }'

Query in a file:
	${me} /path/to/query

Query from pipe:
	echo { query } | ${me}

Enter query:
	${me}
	{ query } (typed, then Control+D)

Set the NODE_PORT environment variable to use something other than the default 8888:
	NODE_PORT=4000 ${me} '{ query }'

	`);
	process.exit(0);
}

if (process.argv[2]) {
	if (/^-*h(elp)?$/i.test(process.argv[2])) {
		usage();
	}
	query(fs.existsSync(process.argv[2]) ? fs.readFileSync(process.argv[2]) : process.argv[2]);
}
else {
	const inp = [];
	process.stdin.resume();
	process.stdin.on('data', (buf) => inp.push(buf));
	process.stdin.on('end', () => {
		query(Buffer.concat(inp));
	});
}

function query(str) {
	request.post({
		'url': `http://localhost:${process.env.NODE_PORT || 8888}/api`,
		'headers': {
			'Content-type': 'application/json'
		},
		'body': JSON.stringify({ 'query': str.toString('utf8') })
	})
	.then(print, print);
}

function print(res) {
	if (res instanceof Error) {
		console.error(res.response ? JSON.stringify(JSON.parse(res.response.body), null, '   ') : res.message);
		process.exit(1);
	}
	console.log(JSON.stringify(JSON.parse(res), null, '   '));
	process.exit(0);
}
