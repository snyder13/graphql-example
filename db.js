/**
 * Loads a temporary database with some data and returns a handle when it's ready.
 *
 * (Using a database manager for data that already exists hard-coded doesn't
 * help much, but I thought SQL would be more illustrative for the example.)
 */

'use strict';
const sqlite = require('sqlite');

// thanks wikipedia
const bands = {
	'Electronic': {
		'members': [
			'Bernard Sumner', 'Johnny Marr', 'Chris Lowe', 'Neil Tennant', 'Karl Bartos'
		],
		'tags': [
			'alternative dance', 'dance-rock', 'alternative rock', 'synthpop'
		]
	},
	'The Smiths': {
		'members': [
			'Morrissey', 'Johnny Marr', 'Andy Rourke', 'Mike Joyce'
		],
		'tags': [
			'indie rock', 'jangle pop'
		]
	},
	'Freebass': {
		'members': [
			'Andy Rourke', 'Peter Hook', 'Gary "Mani" Mounfield', 'Gary Briggs'
		],
		'tags': [
			'alternative rock'
		]
	},
	'The Stone Roses': {
		'members': [
			'Ian Brown', 'John Squire', 'Gary "Mani" Mounfield', 'Alan John "Reni" Wren'
		],
		'tags': [
			'alternative rock', 'indie rock', 'blues rock', 'funk rock', 'dance-rock'
		]
	},
	'New Order': {
		'members': [
			'Bernard Sumner', 'Peter Hook', 'Stephen Morris'
		],
		'tags': [
			'synthpop', 'alternative dance', 'dance-rock', 'post-punk', 'electronica', 'new wave'
		]
	},
	'Joy Division': {
		'members': [
			'Ian Curtis', 'Peter Hook', 'Stephen Morris'
		],
		'tags': [
			'post-punk'
		]
	},
	'Pet Shop Boys': {
		'members': [
			'Chris Lowe', 'Neil Tennant'
		],
		'tags': [
			'synthpop', 'dance-pop', 'art-pop', 'new wave'
		]
	},
	'Kraftwerk': {
		'members': [
			'Ralf Hütter', 'Florian Schneider', 'Wolfgang Flür', 'Karl Bartos'
		],
		'tags': [
			'electronic', 'synth-pop', 'art pop', 'avante-garde', 'krautrock'
		]
	}
};

const talents = {
	'Karl Bartos': [
		'percussion', 'synthesizer', 'vocals'
	],
	'Gary Briggs': [
		'vocals'
	],
	'Ian Brown': [
		'vocals', 'percussion', 'keyboard', 'harmonica', 'guitar', 'bass guitar'
	],
	'Ian Curtis': [
		'vocals', 'guitar', 'melodica', 'synthesizer'
	],
	'Wolfgang Flür': [
		'percussion', 'keyboard'
	],
	'Peter Hook': [
		'bass guitar', 'vocals', 'percussion', 'synthesizer', 'guitar', 'melodica'
	],
	'Ralf Hütter': [
		'synthesizer', 'keyboard', 'vocoder', 'guitar', 'percussion', 'vocals'
	],
	'Mike Joyce': [
		'percussion'
	],
	'Chris Lowe': [
		'synthesizer', 'piano', 'percussion', 'vocals', 'keyboard', 'trombone', 'organ'
	],
	'Johnny Marr': [
		'guitar', 'vocals', 'keyboard', 'piano', 'mandolin', 'autoharp', 'harmonica', 'harmonium'
	],
	'Stephen Morris': [
		'percussion', 'keyboard', 'synthesizer'
	],
	'Morrissey': [
		'vocals'
	],
	'Gary "Mani" Mounfield': [
		'bass guitar', 'synthesizer', 'vocals'
	],
	'Andy Rourke': [
		'bass guitar', 'guitar', 'cello'
	],
	'Florian Schneider': [
		'synthesizer', 'vocals', 'guitar', 'flute', 'saxophone', 'percussion', 'violin'
	],
	'John Squire': [
		'guitar', 'vocals'
	],
	'Bernard Sumner': [
		'vocals', 'guitar', 'keyboard', 'synthesizer', 'melodica'
	],
	'Neil Tennant': [
		'vocals', 'keyboard', 'synthesizer', 'guitar'
	],
	'Alan John "Reni" Wren': [
		'percussion', 'vocals'
	]
};

module.exports = (async () => {
	const dbh = await sqlite.open(':memory:', { Promise });

	// data tables
	for (const type of [ 'bands', 'members', 'tags', 'instruments' ]) {
		await dbh.exec(`CREATE TABLE ${type}(name varchar(50) not null primary key)`);
	}
	// link tables
	for (const [ from, to ] of [ [ 'band', 'member' ], [ 'band', 'tag' ], [ 'instrument', 'member' ] ]) {
		await dbh.exec(`CREATE TABLE ${from}_${to}_rel(${from} varchar(50) not null references ${from}s(name), ${to} varchar(50) not null references ${to}(name))`);
	}

	// populate
	for (const [ band, meta ] of Object.entries(bands)) {
		await dbh.run('INSERT INTO bands(name) VALUES(?)', [ band ]);
		for (const [ type, entries ] of Object.entries(meta)) {
			for (const entry of entries) {
				await dbh.run(`INSERT OR IGNORE INTO ${type}(name) VALUES (?)`, [ entry ]);
				const singular = type.replace(/s$/, '');
				await dbh.run(`INSERT INTO band_${singular}_rel(band, ${singular}) VALUES (?, ?)`, [ band, entry ]);
			}
		}
	}
	for (const [ member, instruments ] of Object.entries(talents)) {
		for (const instrument of instruments) {
			await dbh.run('INSERT OR IGNORE INTO instruments(name) VALUES(?)', [ instrument ]);
			await dbh.run('INSERT INTO instrument_member_rel(instrument, member) VALUES (?, ?)', [ instrument, member ]);
		}
	}
	return dbh;
})();

