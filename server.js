'use strict';
// lightly adapted from http://graphql.org/graphql-js/running-an-express-graphql-server/
const express = require('express');
const graphqlHTTP = require('express-graphql');

require('./graphql').then((graphqlConf) => {
	const app  = express();
	const port = process.env.NODE_PORT || 8888;

	// enable web gui
	graphqlConf.graphiql = true;

	// bind an endpoint
	app.use('/api', graphqlHTTP(graphqlConf));

	// connect express
	app.listen(port, () => console.log(`:${port}`));
});
