'use strict';
module.exports = (async function setup() {
	return {
		'schema':    await require('./schema'),
		'rootValue': await require('./definitions')
	};
})();
