'use strict';
const defaults = {
	'limit': 100,
	'offset': 0
}, limits = {
	'limit': [1, 100],
	'offset': [0, Infinity]
};

function aerate(dbh, type) {
	return (rows) => rows.map((row) => new type(dbh, row));
}

class Table {
	constructor(dbh, props) {
		this.dbh = dbh;
		for (const [key, val] of Object.entries(props)) {
			this[key] = val;
		}
	}

	rel(type, args, filter) {
		const lc = {
			'self': this.constructor.name.toLowerCase(),
			'other': type.name.toLowerCase()
		};
		const relTbl = `${ [ lc.self, lc.other ].sort().join('_') }_rel`;
		const params = [ this.name ];
		let join = '';

		if (filter && args[filter]) {
			join = `INNER JOIN ${ [ lc.other, filter ].sort().join('_') }_rel j ON j.${lc.other} = ${relTbl}.${lc.other} AND j.${filter} = ?`;
			params.unshift(args[filter]);
		}
		return this.dbh.all(`SELECT ${relTbl}.${lc.other} AS name FROM ${relTbl} ${join} WHERE ${lc.self} = ?`, params)
			.then(aerate(this.dbh, type));
	}
}

class Lister {
	static list(dbh, args) {
		args = this.fillDefaults(args);
		const binds = [ args.limit, args.offset ];
		let where = '';
		if (args.name) {
			where = 'WHERE name = ?';
			binds.unshift(args.name);
		}
		return dbh.all(`SELECT name FROM ${this.name.toLowerCase()} ${where} LIMIT ? OFFSET ?`, binds)
			.then(aerate(dbh, this.type))
	}

	static fillDefaults(args) {
		for (const [k, v] of Object.entries(defaults)) {
			if (args[k] === undefined) {
				args[k] = defaults[k];
			}
			if (limits[k]) {
				args[k] = Math.min(limits[k][1], Math.max(limits[k][0], args[k]))
			}
		}
		return args;
	}
}

class Band extends Table {
	tags() { return this.rel(Tag); }
	members(args) { return this.rel(Member, args, 'instrument'); }
}

class Tag extends Table {
	bands() { return this.rel(Band); }
}

class Member extends Table {
	instruments() { return this.rel(Instrument); }
	bands() { return this.rel(Band); }
}

class Instrument extends Table {
	members() { return this.rel(Member); }
}

class Instruments extends Lister {
	static get type() { return Instrument; }
}

class Bands extends Lister {
	static get type() { return Band; }
}

class Tags extends Lister {
	static get type() { return Tag; }
}

class Members extends Lister {
	static get type() { return Member; }
}

// implement accessors to the data in the API
module.exports = require('../db').then((dbh) => {
	const rv = {};
	[ Bands, Tags, Members, Instruments ].forEach((type) => {
		rv[type.name.toLowerCase()] = (args) => type.list(dbh, args)
	});
	return rv;
});
