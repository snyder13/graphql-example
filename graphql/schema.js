'use strict';
const { buildSchema } = require('graphql');

// define what's available through the API
module.exports = buildSchema(`
	type Query {
		bands(limit: Int, offset: Int, name: String): [Band!]!
		tags(limit: Int, offset: Int, name: String): [Tag!]!
		members(limit: Int, offset: Int, name: String): [Member!]!
		instruments(limit: Int, offset: Int, name: String): [Instrument!]!
	}
	type Band {
		name: String!
		tags: [Tag!]!
		members(instrument: String): [Member!]!
	}
	type Tag {
		name: String!
		bands: [Band!]!
	}
	type Member {
		name: String!
		instruments: [Instrument!]!
		bands: [Band!]!
	}
	type Instrument {
		name: String!
		members: [Member!]!
	}
`);
