# GraphQL Example

## About

This branch shows a minimal example of using GraphQL and IQL, the built-in
query explorer & documentation system.

## Install dependencies

	yarn

or

	npm install

## Starting

Use `npm start` to run it on port 8888, or `NODE_PORT=XXXX npm start`

## GraphIQL

The GraphQL package comes with a nice query explorer called GraphIQL, which we
enable in all the examples in this repo.

After starting the server, visit `http://localhost:8888/api` in a browser to
see this interface.

Try entering

	{ bands { name } }

In the left pane and clicking the play button to test. The documentation in the
right pane shows what other types and queries are available.

## Other clients

Anything that can make an HTTP request can query a GraphQL API.

    `$ curl http://localhost:8888/api -q \
    -X POST \
    -H "Content-type: application/json" \
    --data "{ \"query\": \
    \"{ \
      __schema { \
        queryType { \
          fields { \
            name \
            description \
            type { \
              ofType { name kind } \
            } \
          } \
        } \
      } \
    }\" }" | jq`

(`jq` is optional, but it formats the output nicely -- )

    `{
	  "data": {
	    "__schema": {
	      "queryType": {
	        "fields": [
	          {
	            "name": "bands",
	            "description": null,
	            "type": {
	              "ofType": {
	                "name": null,
	                "kind": "LIST"
	              }
	            }
	          },
		...`

By the way, that was an example of a meta query. It's saved in
`introspection.query` if you want to experiment with it. Queries like this
are how GraphQL builds documentation about itself, so you can use it for
related purposes.

You can also use the included client utility to issue queries with less regard
to escaping the string.

`node client introspection.query` (same output)

Run `node client --help` for more usage options.

## References

 * http://graphql.org/graphql-js/running-an-express-graphql-server/
 * http://graphql.org/learn/introspection/

